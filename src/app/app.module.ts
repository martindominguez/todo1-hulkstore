import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TableComponent } from './table/table.component';
import { TableRowComponent } from './table-row/table-row.component';
import { MockService } from './mock-service';
import { HttpClientModule } from '@angular/common/http';
import { PopupComponent } from './addProducts/addProduct.component';
import { PopupService } from './addProducts/addProduct.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddProductRowComponent} from './addProducts/rows/addProduct-row.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovimientosComponent } from './movimientos/movimientos.component';

@NgModule({
  declarations: [
    AppComponent,
    MovimientosComponent,
    TableComponent,
    TableRowComponent,
    PopupComponent,
    AddProductRowComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [MockService, HttpClientModule, PopupService],
  bootstrap: [AppComponent],
  entryComponents: [PopupComponent]
})
export class AppModule { }
