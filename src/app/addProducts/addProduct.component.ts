import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NameValuePair } from '../domain/nameValuePair';
import { Observable } from 'rxjs';
import { MockService } from '../mock-service';

@Component({
    selector: 'my-popup',
    templateUrl: './addProduct.component.html',
    host: {
        '[@state]': 'state',
    },
    animations: [
        trigger('state', [
            state('opened', style({ transform: 'translateY(0%)' })),
            state('void, closed', style({ transform: 'translateY(100%)', opacity: 0 })),
            transition('* => *', animate('100ms ease-in')),
        ])
    ],
    styleUrls: ['./addProduct.css']
})


export class PopupComponent implements OnInit {
    private state: 'opened' | 'closed' = 'closed';

    private data: any = [];

    characters: Observable<any[]>;
    columns: NameValuePair[];


    @Input() set message(message: string) {
        this._message = message;
        this.state = 'opened';
    }

    get message(): string {
        return this._message;
    }
    _message: string;

    @Output() closed = new EventEmitter();

    constructor(private atService: MockService) { }

    ngOnInit() {
        this.columns = this.atService.getBuyColumns();
        this.characters = this.atService.getCharacters();
        this.characters.subscribe(element => {
            this.data = element.map(aux => {
                return { amount: 0 };
            });
        });
    }

    guardaCompra() {
        this.data.forEach(element => {
            alert(element.amount);
        })
    }
}
