import { Component, OnInit, Input } from '@angular/core';
import { MockService } from '../../mock-service';

@Component({
    selector: '[app-addProduct-row]',
    templateUrl: './addProduct-row.component.html',
    styleUrls: ['./addProduct-row.component.css']
})
export class AddProductRowComponent implements OnInit {
    @Input() character: any;
    @Input() columns: string[];
    @Input() index: number;
    @Input() amount: any;

    constructor(private atService: MockService) { }
    ngOnInit() {
    }
}
