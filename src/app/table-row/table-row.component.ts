import { Component, OnInit, Input } from '@angular/core';
import { MockService } from '../mock-service';
import { Router } from '@angular/router';

@Component({
  selector: '[app-table-row]',
  templateUrl: './table-row.component.html',
  styleUrls: ['./table-row.component.css']
})
export class TableRowComponent implements OnInit {
  @Input() character: any;
  @Input() columns: string[];
  @Input() index: number;
  constructor(private atService: MockService, private router: Router) { }
  ngOnInit() { }

  agregarProducto(id) {
    this.atService.getCharacters().subscribe(response => {
      const current = response.find(item => {
        return item.id === id;
      });
      this.atService.addMovement(current, 1, 'Fc453246');
      console.log(this.atService.getAllMovements());
    });
  }

  verMovimientos() {
    this.router.navigate(['movimientos']);
  }
}
