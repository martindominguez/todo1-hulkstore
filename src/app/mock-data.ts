export const CHARACTERS: any[] =
[
  {
    id: 1,
    name: 'comic',
    price: 1500,
    stock: 50
  },
  {
    id: 2,
    name: 'comic2',
    price: 420,
    stock: 100
  },
  {
    id: 3,
    name: 'comic3',
    price: 3200,
    stock: 3
  },
  {
    id: 4,
    name: 'comic4',
    price: 250,
    stock: 1
  }
];
