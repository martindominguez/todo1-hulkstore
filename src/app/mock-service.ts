import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { CHARACTERS } from './mock-data';
import { Movement } from './domain/movement';
import { NameValuePair } from './domain/nameValuePair';

@Injectable()
export class MockService {
  private movements: Array<Movement>;
  constructor() { }

  getCharacters(): Observable<any[]> {
    return Observable.of(CHARACTERS).delay(100);
  }
  
  getBuyColumns(): NameValuePair[] {
    return [new NameValuePair('Id', 'id'),
    new NameValuePair('Nombre', 'name'),
    new NameValuePair('Precio', 'price'),
    new NameValuePair('Cantidad', 'count')];
  }

  getColumns(): NameValuePair[] {
    return [new NameValuePair('Id', 'id'),
    new NameValuePair('Nombre', 'name'),
    new NameValuePair('Precio', 'price'),
    new NameValuePair('Stock', 'stock')];
  }

  getAllMovements(): Array<any> {
    return this.movements;
  }

  getMovementsWithId(id): Array<Movement> {
    const movementsId = new Array();
    this.movements.forEach(element => {
      if (element.productId === id) {
        movementsId.push(element);
      }
    });

    return movementsId;
  }

  addMovement(product, count, description) {
    if (!this.movements) {
      this.movements = [];
    }
    const movement = new Movement(product.id, product.price, count, description);

    this.movements.push(movement);
  }

  removeAllMovements() {
    this.movements = [];
  }
}
