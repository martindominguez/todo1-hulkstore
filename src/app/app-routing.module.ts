import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MovimientosComponent } from './movimientos/movimientos.component'

const appRoutes: Routes = [
  {
    path: '',
    component: AppComponent
  },
  {
    path: 'movimientos',
    component: MovimientosComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
