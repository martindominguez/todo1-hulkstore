export class Movement {
    productId: number;
    price: number;
    count: number;
    description: string;

    constructor(productId: number, price: number, count: number, description: string) {
        this.productId = productId;
        this.price = price;
        this.count = count;
        this.description = description;
    }
}
