import { Component, OnInit, Injector } from '@angular/core';
import { MockService } from '../mock-service';
import { Observable } from 'rxjs/Observable';
import { NameValuePair } from '../domain/nameValuePair';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  characters: Observable<any[]>;
  columns: NameValuePair[];
  constructor(private atService: MockService) { }

  ngOnInit() {
    this.columns = this.atService.getColumns();
    this.characters = this.atService.getCharacters();
  }

}
